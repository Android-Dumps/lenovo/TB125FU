## full_P98980AA1-user 12 SP1A.210812.016 TB125FU_USR_S000160_2302271447_MP1RC_ROW release-keys
- Manufacturer: lenovo
- Platform: mt6768
- Codename: TB125FU
- Brand: Lenovo
- Flavor: full_P98980AA1-user
- Release Version: 12
- Kernel Version: 4.14.186
- Id: SP1A.210812.016
- Incremental: TB125FU_USR_S000160_2302271447_MP1RC_ROW
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: undefined
- Screen Density: undefined
- Fingerprint: Lenovo/TB125FU/TB125FU:12/SP1A.210812.016/S000160_230227_ROW:user/release-keys
- OTA version: 
- Branch: full_P98980AA1-user-12-SP1A.210812.016-TB125FU_USR_S000160_2302271447_MP1RC_ROW-release-keys
- Repo: lenovo/TB125FU
